import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:urben/screens/analytics_screen.dart';
import 'package:urben/screens/home_screen.dart';
import 'package:urben/screens/community_screen.dart';
import 'package:urben/screens/login_screen.dart';
import 'package:urben/screens/promote_screen.dart';
import 'package:urben/screens/products_screen.dart';

class MainScreen extends StatefulWidget {
  @override
  _MainScreenState createState() => _MainScreenState();
}

class _MainScreenState extends State<MainScreen> {

  SharedPreferences sharedPreferences;

  @override
  void initState(){
    super.initState();
    checkLoginStatus();
  }

  checkLoginStatus() async {
    sharedPreferences = await SharedPreferences.getInstance();
    if(sharedPreferences.getString("token")==null){
      Navigator.of(context).pushAndRemoveUntil(MaterialPageRoute(
          builder: (BuildContext context) => LoginScreen()), (
          Route<dynamic> route) => false);
    }
    }

  int _currentIndex = 0;
  Widget callPage(int currentIndex) {
    switch (currentIndex) {
      case 0:
        return HomeScreen();
      case 1:
        return ProductsScreen();
      case 2:
        return PromoteScreen();
      case 3:
        return AnalyticsScreen();
      case 4:
        return CommunityScreen();
        break;
      default:
        return LoginScreen();
    }
  }

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 1,
      child: Scaffold(
          appBar: AppBar(
            bottom: TabBar(
              tabs: <Widget>[
                TextField(
                  cursorColor: Colors.white,
                  style: TextStyle(color: Colors.white),
                  decoration: InputDecoration(
                      prefixIcon: Icon(Icons.search, color: Colors.white),
                      hintText: "What's in your mind",
                      hintStyle: TextStyle(color: Colors.white)),
                ),
              ],
            ),
            backgroundColor: Colors.black,
            automaticallyImplyLeading: false,
            title: GestureDetector(
              onTap: () {
                Navigator.of(context).push(MaterialPageRoute(
                  builder: (context) => MainScreen(),
                ));
              },
              child: Row(children: [
                Icon(
                  FontAwesomeIcons.mapMarkerAlt,
                  color: Colors.red,
                ),
                Column(children: [
                  Text(
                    'Nearby',
                    style: TextStyle(fontSize: 12, color: Colors.white60),
                  ),
                  Text(
                    'tunis',
                    style: TextStyle(fontSize: 12),
                  )
                ])
              ]),
            ),
            actions: <Widget>[
              Row(
                children: <Widget>[
                  Icon(FontAwesomeIcons.signOutAlt),
                  SizedBox(
                    width: 5,
                  ),
                  GestureDetector(
                      onTap: () async {
                        sharedPreferences = await SharedPreferences.getInstance();
                        sharedPreferences.clear();
                        Navigator.of(context).pushAndRemoveUntil(MaterialPageRoute(
                            builder: (BuildContext context) => LoginScreen()), (
                            Route<dynamic> route) => false);
                      },
                      child: Text('Sign Out')),
                  SizedBox(
                    width: 15,
                  )
                ],
              )
            ],
          ),
          bottomNavigationBar: BottomNavigationBar(
            currentIndex: _currentIndex,
            type: BottomNavigationBarType.fixed,
            selectedItemColor: Colors.red,
            items: [
              BottomNavigationBarItem(
                  icon: Icon(FontAwesomeIcons.home),
                  title: Text('Home'),
                  backgroundColor: Colors.white),
              BottomNavigationBarItem(
                  icon: Icon(FontAwesomeIcons.storeAlt,),
                  title: Text('Products'),
                  backgroundColor: Colors.white),
              BottomNavigationBarItem(
                  icon: Icon(FontAwesomeIcons.bullhorn),
                  title: Text('Promote'),
                  backgroundColor: Colors.white),
              BottomNavigationBarItem(
                  icon: Icon(FontAwesomeIcons.solidChartBar),
                  title: Text('Analytics'),
                  backgroundColor: Colors.white),
              BottomNavigationBarItem(
                  icon: Icon(FontAwesomeIcons.userCircle),
                  title: Text('Community'),
                  backgroundColor: Colors.white),
            ],
            onTap: (index) {
              setState(() {
                _currentIndex = index;
              });
            },
          ),
          body: callPage(_currentIndex)),
    );
  }
}
