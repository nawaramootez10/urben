import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:urben/screens/auth_screen.dart';

class SplashScreen extends StatelessWidget {
  const SplashScreen({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: GestureDetector(
        onTap: (){Navigator.of(context).push(MaterialPageRoute(builder: (context) => AuthScreen()));},
        child: Container(
          height: double.infinity,
          decoration: BoxDecoration(
              image: DecorationImage(
                  image: AssetImage('assets/images/urben.png'),
                  fit: BoxFit.cover
              )
          ),
        ),
      ),
    );
  }
}