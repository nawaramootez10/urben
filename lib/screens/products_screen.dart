import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:urben/models/product.dart';
import 'package:urben/screens/addProduct_screen.dart';
import 'package:http/http.dart' as http;

class ProductsScreen extends StatefulWidget {
  @override
  _ProductsScreenState createState() => _ProductsScreenState();
}

class _ProductsScreenState extends State<ProductsScreen> {
  var authToken, productName, productDescription, productCategory, productTags, productImage, productInitialPrice, productOnSalePrice, productVisibility, status;

  List unfilterData;
  Future<List<Product>> _getProducts() async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    authToken = sharedPreferences.getString("token");
    print(authToken);
    var data = await http.get(
        "http://10.0.2.2:5000/api/v1/test",
        headers: {'Authorization': 'Bearer '+authToken});
    var jsonData = json.decode(data.body);
    this.unfilterData = jsonData;
    print(jsonData);
    List<Product> products = [];
    for (var u in jsonData) {
      Product product = Product(u["productName"], u["productDescription"], u["productCategory"], u["productTags"], u["productImage"],
          u["productInitialPrice"], u["productOnSalePrice"], u["productVisibility"], u["status"] );
      products.add(product);
    }
    print(products.length);
    return products;
  }

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          automaticallyImplyLeading: false,
          title: Text(
          " My Products ",
          style: TextStyle(color: Colors.black, fontWeight: FontWeight.bold),
          ),
          backgroundColor: Colors.white,
          elevation: 2,
        ),
      body: new Container(
        margin: EdgeInsets.all(10.0),
        child: FutureBuilder(
          future: _getProducts(),
          builder: (BuildContext context, AsyncSnapshot snapshot) {
            if (snapshot.data == null) {
              return new Container(
                  margin: const EdgeInsets.only(top: 100.0),
                  child: new Center(
                      child: new Column(
                        children: <Widget>[
                          new CircularProgressIndicator(strokeWidth: 4.0),
                          new Container(
                            padding: const EdgeInsets.all(8.0),
                            child: new Text(
                              'Chargement',
                              style: new TextStyle(
                                  color: Colors.blueGrey.shade300, fontSize: 16.0),
                            ),
                          )
                        ],
                      )
                  )
              );
            } else {
              if (snapshot.data.length == 0) {
                return Container(
                  margin: const EdgeInsets.all(50.0),
                  child: Text("Aucun produit", style: TextStyle(
                    color: Colors.red,
                    fontWeight: FontWeight.w500,
                    fontSize: 22,),),
                );
              } else {
                return Scrollbar(
                  child: new Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: GridView.builder(
                      shrinkWrap: true,
                      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                          crossAxisCount: 1),
                      itemBuilder: (BuildContext context, int index) {
                        return Padding(
                            padding: EdgeInsets.all(5.0),
                            child: GestureDetector(
                              onTap: () {
                                Navigator.push(context, MaterialPageRoute(
                                    builder: (context) =>
                                        Detail(snapshot.data[index])));
                              },
                              child: Container(
                                  decoration: BoxDecoration(
                                      color: Colors.white,
                                      shape: BoxShape.rectangle,
                                      borderRadius: BorderRadius.circular(20.0),
                                      boxShadow: [
                                        BoxShadow(
                                            color: Colors.black12,
                                            blurRadius: 8.0)
                                      ]),
                                  child: Column(
                                    crossAxisAlignment: CrossAxisAlignment
                                        .start,
                                    children: <Widget>[
                                      Container(
                                        height: 200.0,
                                        padding: EdgeInsets.all(10.0),
                                        child: Row(
                                          crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                          children: <Widget>[
                                            Expanded(
                                              child: Container(
                                                child: new ClipRRect(
                                                  borderRadius:
                                                  new BorderRadius
                                                      .circular(15.0),
                                                  child: Image.network(
                                                    // snapshot
                                                    //     .data[index].productImage,
                                                    "https://images.unsplash.com/photo-1476170434383-88b137e598bb?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1950&q=80",
                                                    fit: BoxFit.cover,
                                                    height: 200.0,
                                                    width: 500.0,
                                                  ),
                                                ),
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                      SizedBox(
                                        height: 3.0,
                                      ),
                                      Padding(
                                        padding: EdgeInsets.only(
                                          left: 15.0,
                                          right: 15.0,),
                                        child: Column(
                                          crossAxisAlignment: CrossAxisAlignment
                                              .start,
                                          children: <Widget>[
                                            Text(
                                              snapshot.data[index].productName,
                                              style: TextStyle(
                                                  fontWeight: FontWeight.w600,
                                                  fontSize: 20.0,
                                                  color: Colors.teal),
                                            ),
                                            SizedBox(height: 10,),
                                            Text(
                                              "Category:  " +
                                                  snapshot.data[index]
                                                      .productCategory,
                                              style: TextStyle(
                                                  fontWeight: FontWeight.w600,
                                                  fontSize: 18.0,
                                                  color: Colors.grey),
                                            ),
                                          ],
                                        ),
                                      ),
                                    ],
                                  )),
                            ));
                      },
                      itemCount: snapshot.data.length,
                    ),
                  ),
                );
              }
            }
          },
        ),
      ),
    );
  }
}

class Detail extends StatelessWidget {
  static final String routeName = 'detail';
  final Product product;

  String status;

  Detail(this.product);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: new AppBar(
        backgroundColor: Colors.blue,
      ),
      body: new Container(
        child: new ListView(
          children: <Widget>[
            SizedBox(height: 10.0),
            Text(
              product.productName,
              style: TextStyle(color: Colors.black87, fontSize: 28.0, fontWeight: FontWeight.bold),
            ),
            SizedBox(height: 30.0),
            Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                Flexible(
                  child:Container(
                    padding: const EdgeInsets.all(10.0),
                    decoration: new BoxDecoration(
                        border: new Border.all(color: Colors.grey),
                        borderRadius: BorderRadius.circular(10.0)),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text(
                          'Product Description : ',
                          maxLines: 10,
                          style: TextStyle(color: Colors.lightBlue, fontSize: 17),
                        ),
                        Text(product.productDescription, style: TextStyle(color: Colors.black, fontSize: 15),),
                      ],
                    ),
                  ),
                ),
              ],
            ),
            Container(
              padding:
              const EdgeInsets.only(top: 30.0, left: 10.0, right: 10.0),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(
                    'Product Category :  ',
                    maxLines: 10,
                    style: TextStyle(color: Colors.lightBlue, fontSize: 17),
                  ),
                  Text(product.productCategory, style: TextStyle(color: Colors.black, fontSize: 15),),
                ],
              ),
            ),
            Container(
              padding:
              const EdgeInsets.only(top: 40.0, left: 10.0, right: 10.0),
              child: new Container(
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text(
                      'Product Tags :  ',
                      maxLines: 10,
                      style: TextStyle(color: Colors.lightBlue, fontSize: 17),
                    ),
                    Text(product.productTags, style: TextStyle(color: Colors.black, fontSize: 15),),
                  ],
                ),
              ),
            ),
            // Container(
            //   padding:
            //   const EdgeInsets.only(top: 40.0, left: 10.0, right: 10.0),
            //   child: new Container(
            //     child: Row(
            //       crossAxisAlignment: CrossAxisAlignment.start,
            //       children: <Widget>[
            //         Text(
            //           'Product Initial Price :  ',
            //           maxLines: 10,
            //           style: TextStyle(color: Colors.lightBlue, fontSize: 17),
            //         ),
            //         Text(product.productInitialPrice.toString(), style: TextStyle(color: Colors.black, fontSize: 15),),
            //       ],
            //     ),
            //   ),
            // ),
            // Container(
            //   padding:
            //   const EdgeInsets.only(top: 40.0, left: 10.0, right: 10.0),
            //   child: new Container(
            //     child: Row(
            //       crossAxisAlignment: CrossAxisAlignment.start,
            //       children: <Widget>[
            //         Text(
            //           'Product On Sale Price :  ',
            //           maxLines: 10,
            //           style: TextStyle(color: Colors.lightBlue, fontSize: 17),
            //         ),
            //         Text(product.productOnSalePrice.toString(), style: TextStyle(color: Colors.black, fontSize: 15),),
            //       ],
            //     ),
            //   ),
            // ),
            Container(
              padding:
              const EdgeInsets.only(top: 40.0, left: 10.0, right: 10.0),
              child: new Container(
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text(
                      'Product Visibility :  ',
                      maxLines: 10,
                      style: TextStyle(color: Colors.lightBlue, fontSize: 17),
                    ),
                      Text(product.productVisibility,
                        style: TextStyle(color: Colors.black, fontSize: 15),),
                  ],
                ),
              ),
            ),
            Container(
              padding:
              const EdgeInsets.only(top: 40.0, left: 10.0, right: 10.0),
              child: new Container(
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text(
                      'Promote Status :  ',
                      maxLines: 10,
                      style: TextStyle(color: Colors.lightBlue, fontSize: 17),
                    ),
                    Text(product.status.toString(), style: TextStyle(color: Colors.black, fontSize: 15),),
                  ],
                ),
              ),
            ),
          ],
        ),
        margin: EdgeInsets.all(25.0),
      ),
    );
  }
}