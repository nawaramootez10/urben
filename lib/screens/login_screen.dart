import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:urben/components/main_screen.dart';
import 'package:urben/screens/signup_screen.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';

class LoginScreen extends StatefulWidget {
  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  TextEditingController emailController = new TextEditingController();
  TextEditingController passwordController = new TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Container(
          padding: EdgeInsets.all(20.0),
          color: Color(0xff242633),
          child: ListView(
            children: <Widget>[
              SizedBox(height: 150,),
              Text(
                'Urben',
                style: TextStyle(
                  fontSize: 70,
                  fontWeight: FontWeight.w300,
                  color: Colors.red.shade700,
                ),
                textAlign: TextAlign.center,
              ),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  SizedBox(height: 90,),
                  Text(
                      'Sign In',
                      style: TextStyle(
                          fontSize: 40,
                          fontWeight: FontWeight.w400,
                          color: Colors.white),
                  ),
                  SizedBox(height: 20,),
                  ListTile(
                      title: TextField(
                        controller: emailController,
                        cursorColor: Colors.white,
                        style: TextStyle(color: Colors.white),
                        decoration: InputDecoration(
                            hintText: "Email address",
                            hintStyle: TextStyle(color: Colors.white38),
                            border: InputBorder.none,
                            icon: Icon(Icons.person_outline, color: Colors.white,)
                        ),
                      )
                  ),
                  Divider(color: Colors.white,),
                  SizedBox(height: 20,),
                  ListTile(
                      title: TextField(
                        controller: passwordController,
                        obscureText: true,
                        cursorColor: Colors.white,
                        style: TextStyle(color: Colors.white),
                        decoration: InputDecoration(
                            hintText: "Password",
                            hintStyle: TextStyle(color: Colors.white38),
                            border: InputBorder.none,
                            icon: Icon(Icons.lock_outline, color: Colors.white,)
                        ),
                      )
                  ),
                  Divider(color: Colors.white,),
                  SizedBox(height: 70,),
                  SizedBox(
                    width: double.infinity,
                        child: RaisedButton(
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(5.0),
                              side: BorderSide(color: Colors.lightBlueAccent,width: 2.5,style: BorderStyle.solid)
                          ),
                          padding: const EdgeInsets.all(20),
                          onPressed: (){
                            login(emailController.text, passwordController.text);
                          },
                          color: Color(0xff242633),
                          child: Text('SIGN IN', style: TextStyle(color: Colors.lightBlueAccent, fontSize: 18, fontWeight: FontWeight.w400),),
                        ),
                      ),
                  SizedBox(height: 10,),
                  SizedBox(
                    width: double.infinity,
                    child: RaisedButton(
                      onPressed: (){Navigator.of(context).push(MaterialPageRoute(builder: (context) => SignUpScreen()));},
                      color: Color(0xff242633),
                      child: Text("Don't have an account?", style: TextStyle(color: Colors.white, fontSize: 16),),
                    ),
                  ),
                ],
              ),
            ],
          ),
        )
    );
  }
  login(String email,String password) async {
    Map data = {
      'email': email,
      'password': password
    };
    var jsonData;
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    var response = await http.post(
        "http://10.0.2.2:5000/api/v1/login", body: data);
    if (response.statusCode == 200) {
      jsonData = json.decode(response.body);
      setState(() {
        sharedPreferences.setString("token", jsonData['token']);
        Navigator.of(context).pushAndRemoveUntil(MaterialPageRoute(
            builder: (BuildContext context) => MainScreen()), (
            Route<dynamic> route) => false);
      });
    }
    else{
      Fluttertoast.showToast(msg: "error");
    }
  }
}