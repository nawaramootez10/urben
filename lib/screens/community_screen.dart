import 'dart:convert';
import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:http/http.dart' as http;
import 'package:fluttertoast/fluttertoast.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:urben/components/main_screen.dart';
import 'package:urben/screens/login_screen.dart';

class CommunityScreen extends StatefulWidget {
  @override
  _CommunityScreenState createState() => _CommunityScreenState();
}

class _CommunityScreenState extends State<CommunityScreen> {
  SharedPreferences sharedPreferences;
  var _authToken;

  @override
  void initState(){
    super.initState();
    checkLoginStatus();
  }

  checkLoginStatus() async {
    sharedPreferences = await SharedPreferences.getInstance();
    if(sharedPreferences.getString("token")==null){
      Navigator.of(context).pushAndRemoveUntil(MaterialPageRoute(
          builder: (BuildContext context) => LoginScreen()), (
          Route<dynamic> route) => false);
    }
    _authToken = sharedPreferences.getString("token");
  }

  TextEditingController customerNameController = new TextEditingController();
  TextEditingController customerEmailController = new TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        title: Text((' Community '.toUpperCase()),
            style: TextStyle(fontSize: 20, color: Colors.black),),
      ),
      body: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            SizedBox(
              height: 120,
            ),
            Padding(
              padding: EdgeInsets.only(left: 50, right: 50),
              child: Container(
                padding: EdgeInsets.all(20.0),
                height: 230,
                width: 350,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.all(Radius.circular(15)),
                  gradient: LinearGradient(
                    begin: Alignment(1.0, -1.0),
                    end: Alignment(-1.0, 1.0),
                    colors: [const Color(0xffd0ffae), const Color(0xff34ebe9)],
                    stops: [0.0, 1.0],
                  ),
                ),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    Text('Customer Name'.toUpperCase(), style: TextStyle(fontSize: 18, fontWeight: FontWeight.w700, color: Colors.blue.shade700),),
                    TextField(
                      controller: customerNameController,
                      style: TextStyle(color: Colors.black, fontWeight: FontWeight.w500, fontSize: 16),
                      cursorColor: Colors.teal,
                      decoration: InputDecoration(
                        enabledBorder: OutlineInputBorder(
                            borderSide: BorderSide(color: Colors.grey.shade800),
                            borderRadius:
                            BorderRadius.all(Radius.circular(10))),
                        hintText: 'customer name'.toUpperCase(),
                        hintStyle: TextStyle(color: Colors.grey.shade800),
                      ),
                    ),
                    Text('Customer Email'.toUpperCase(), style: TextStyle(fontSize: 18, fontWeight: FontWeight.w700, color: Colors.blue.shade700),),
                    TextField(
                      controller: customerEmailController,
                      style: TextStyle(color: Colors.black, fontWeight: FontWeight.w500, fontSize: 16),
                      cursorColor: Colors.teal,
                      decoration: InputDecoration(
                        enabledBorder: OutlineInputBorder(
                            borderSide: BorderSide(color: Colors.grey.shade800),
                            borderRadius:
                            BorderRadius.all(Radius.circular(10))),
                        hintText: 'customer email'.toUpperCase(),
                        hintStyle: TextStyle(color: Colors.grey.shade800),
                      ),
                    )
                  ]),
                ),
              ),
            Padding(
              padding: const EdgeInsets.all(30.0),
              child: Container(
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.all(Radius.circular(15)),
                  gradient: LinearGradient(
                    begin: Alignment(1.0, -1.0),
                    end: Alignment(-1.0, 1.0),
                    colors: [Colors.grey, Colors.blueGrey],
                    stops: [0.0, 1.0],
                  ),
                ),
                child: FlatButton(
                  //color: Colors.red,
                  textColor: Colors.white,
                  padding: const EdgeInsets.only(
                      right: 80, left: 80, top: 15, bottom: 15),
                  onPressed: () {
                    addCustomer(customerNameController.text, customerEmailController.text);
                  },
                  child: Text(
                    'add customer'.toUpperCase(),
                    style: TextStyle(fontSize: 12.5),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  addCustomer(String customerName,String customerEmail) async {
    Map data = {
      'customerName': customerName,
      'customerEmail': customerEmail
    };
    var jsonData;
    var response = await http.post(
        "http://10.0.2.2:5000/api/v1/community", body: data, headers: {'Authorization': 'Bearer ' + _authToken});
    if (response.statusCode == 200) {
      jsonData = json.decode(response.body);
      Fluttertoast.showToast(
          msg: 'Customer added successfully',
          toastLength: Toast.LENGTH_SHORT ,
          gravity: ToastGravity.BOTTOM ,
          timeInSecForIosWeb: 15 ,
          backgroundColor: Colors.black ,
          textColor: Colors.white ,
          fontSize: 16.0
      );
      setState(() {
        Navigator.of(context).pushAndRemoveUntil(MaterialPageRoute(
            builder: (BuildContext context) => MainScreen()), (
            Route<dynamic> route) => false);
      });
    }
    else{
      Fluttertoast.showToast(msg: "error");
    }
  }
}
