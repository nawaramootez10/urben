import 'dart:convert';
import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:image_picker/image_picker.dart';
import 'package:path/path.dart' as Path;
import 'package:shared_preferences/shared_preferences.dart';
import 'package:urben/components/main_screen.dart';
import 'package:http/http.dart' as http;
import 'package:group_radio_button/group_radio_button.dart';
import 'package:urben/screens/login_screen.dart';

class AddProductScreen extends StatefulWidget {
  @override
  _AddProductScreenState createState() => _AddProductScreenState();
}

class _AddProductScreenState extends State<AddProductScreen> {

  File productImage;
  String imageString;
  int selectedRadio;
  SharedPreferences sharedPreferences;
  var _authToken;

  @override
  void initState(){
    super.initState();
    checkLoginStatus();
  }

  checkLoginStatus() async {
    sharedPreferences = await SharedPreferences.getInstance();
    if(sharedPreferences.getString("token")==null){
      Navigator.of(context).pushAndRemoveUntil(MaterialPageRoute(
          builder: (BuildContext context) => LoginScreen()), (
          Route<dynamic> route) => false);
    }
    _authToken = sharedPreferences.getString("token");
    print(_authToken);
  }

  final productNameController = new TextEditingController();
  final productDescriptionController = new TextEditingController();
  final productCategoryController = new TextEditingController();
  final productTagsController = new TextEditingController();
  final emailController = new TextEditingController();
  final productInitialPriceController = new TextEditingController();
  final productOnSalePriceController = new TextEditingController();
  final productVisibilityController = new TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Container(
          padding: EdgeInsets.all(20.0),
          color: Colors.white,
          child: ListView(
            children: <Widget>[
              Column(
                children: <Widget>[
                  SizedBox(height: 15),
                  Text('product/service management'.toUpperCase(),
                      style: TextStyle(
                          fontSize: 22,
                          fontWeight: FontWeight.w500,
                          color: Colors.black)),
                  SizedBox(
                    height: 40,
                  ),
                  ListTile(
                      title: TextField(
                        controller: productNameController,
                        cursorColor: Colors.black,
                        style: TextStyle(color: Colors.black),
                        decoration: InputDecoration(
                            hintText: "Product Name",
                            hintStyle: TextStyle(color: Colors.grey),
                            enabledBorder: OutlineInputBorder(
                                borderSide: BorderSide(color: Colors.grey),
                                borderRadius: BorderRadius.all(Radius.circular(10))),
                            icon: Icon(
                              FontAwesomeIcons.store,
                              color: Colors.grey.shade600,
                            )),
                      )),
                  // Divider(color: Colors.grey.shade600,),
                  ListTile(
                      title: TextField(
                        controller: productDescriptionController,
                        cursorColor: Colors.black,
                        style: TextStyle(color: Colors.black),
                        decoration: InputDecoration(
                            hintText: "Product Description",
                            hintStyle: TextStyle(color: Colors.grey),
                            enabledBorder: OutlineInputBorder(
                                borderSide: BorderSide(color: Colors.grey),
                                borderRadius: BorderRadius.all(Radius.circular(10))),
                            icon: Icon( FontAwesomeIcons.fileArchive, color: Colors.grey,)
                        ),
                      )),
                  ListTile(
                      title: TextField(
                        controller: productCategoryController,
                        cursorColor: Colors.white,
                        style: TextStyle(color: Colors.black),
                        decoration: InputDecoration(
                            hintText: "Product Category",
                            hintStyle: TextStyle(color: Colors.grey),
                            enabledBorder: OutlineInputBorder(
                                borderSide: BorderSide(color: Colors.grey),
                                borderRadius: BorderRadius.all(Radius.circular(10))),
                            icon: Icon( FontAwesomeIcons.searchPlus, color: Colors.grey,)
                        ),
                      )
                  ),
                  ListTile(
                      title: TextField(
                        controller: productTagsController,
                        cursorColor: Colors.white,
                        style: TextStyle(color: Colors.black),
                        decoration: InputDecoration(
                            hintText: "Product tags",
                            hintStyle: TextStyle(color: Colors.grey),
                            enabledBorder: OutlineInputBorder(
                                borderSide: BorderSide(color: Colors.grey),
                                borderRadius: BorderRadius.all(Radius.circular(10))),
                            icon: Icon(FontAwesomeIcons.tags,color: Colors.grey, )
                        ),
                      )
                  ),
                  SizedBox(height: 20),
                  FlatButton(
                    onPressed: () {getImage();
                      imageString=Path.basename(productImage.path);
                    },
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[
                        CircleAvatar(
                            backgroundColor: Colors.pink,
                            child: Icon(
                              Icons.add_photo_alternate,
                              color: Colors.white,
                            )),
                        SizedBox(width: 20,),
                        imageString == null ?
                        Text("Pick new Image",
                            style: TextStyle(fontSize: 15),
                          ):
                          Text(imageString,
                            style: TextStyle(fontSize: 15),
                          ),
                      ],
                    ),
                  ),
                  SizedBox(height: 20),
                  ListTile(
                      title: TextField(
                        keyboardType: TextInputType.number,
                        controller: productInitialPriceController,
                        cursorColor: Colors.white,
                        style: TextStyle(color: Colors.black),
                        decoration: InputDecoration(
                            hintText: "Product Initial Price ",
                            hintStyle: TextStyle(color: Colors.grey),
                            enabledBorder: OutlineInputBorder(
                                borderSide: BorderSide(color: Colors.grey),
                                borderRadius: BorderRadius.all(Radius.circular(10))),
                            icon: Icon( FontAwesomeIcons.solidMoneyBillAlt,color: Colors.grey,)
                        ),
                      )),
                  ListTile(
                      title: TextField(
                        keyboardType: TextInputType.number,
                        controller: productOnSalePriceController,
                        cursorColor: Colors.black,
                        style: TextStyle(color: Colors.black),
                        decoration: InputDecoration(
                            hintText: "Product on sale price:",
                            hintStyle: TextStyle(color: Colors.grey),
                            enabledBorder: OutlineInputBorder(
                                borderSide: BorderSide(color: Colors.grey),
                                borderRadius: BorderRadius.all(Radius.circular(10))),
                            icon: Icon(FontAwesomeIcons.commentsDollar, color: Colors.grey,)
                        ),
                      )
                  ),
                Row(
                  children: [
                    SizedBox(width: 16,),
                    Icon (FontAwesomeIcons.eye, color: Colors.grey),
                    SizedBox(width: 30,),
                    RadioGroup<String>.builder(
                      direction: Axis.horizontal,
                      groupValue: productVisibilityController.text,
                      onChanged: (value) => setState(() {
                        productVisibilityController.text = value;
                      }),
                      items: ["Published", "UnPublished"],
                      itemBuilder: (item) => RadioButtonBuilder(
                        item,
                      ),
                    ),
                  ],
                ),
                  SizedBox(height: 40),
                  Row(
                    children: <Widget>[
                      Expanded(
                        child: RaisedButton(
                          padding: const EdgeInsets.all(18.0),
                          onPressed: () {
                            addProduct(productNameController.text, productDescriptionController.text, productCategoryController.text,
                                productTagsController.text, 'https://images.unsplash.com/photo-1599072648599-eb5335feccd2?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=500&q=60',int.parse(productInitialPriceController.text), int.parse(productInitialPriceController.text),
                                productVisibilityController.text);
                          },
                          color: Colors.blue,
                          child: Row(
                              mainAxisSize: MainAxisSize.max,
                              children: [
                                SizedBox(width: 70,),
                                Icon(
                                  Icons.save,
                                  color: Colors.white,
                                ),
                                SizedBox(width: 20,),
                                Text(
                                  'Save product'.toUpperCase(),
                                  style: TextStyle(
                                      color: Colors.white, fontSize: 16.0),
                                )
                              ]),
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ],
          ),
        )
    );
  }

  Future getImage() async {
    // Get image from gallery.
    var image = await ImagePicker.pickImage(source: ImageSource.gallery);
    setState(() {
      productImage = image;
      print('Image name: $productImage');
    });
  }

  addProduct(String productName, String productDescription, String productCategory,
      String productTags, String productImage,int productInitialPrice, int productOnSalePrice, String productVisibility) async {
    Map data = {
      'productName': productName,
      'productDescription': productDescription,
      'productCategory': productCategory,
      'productTags': productTags,
      'productImage': productImage,
      'productInitialPrice': productInitialPrice,
      'productOnSalePrice': productOnSalePrice,
      'productVisibility': productVisibility
    };
    var jsonData;
    var response = await http.post(
        "http://10.0.2.2:5000/api/v1/product",body:  data, headers: {'Authorization': "Bearer " + _authToken});
    if (response.statusCode == 200) {
      jsonData = json.decode(response.body);
      setState(() {
        Fluttertoast.showToast(
            msg: 'Product added successfully',
            toastLength: Toast.LENGTH_SHORT ,
            gravity: ToastGravity.BOTTOM ,
            timeInSecForIosWeb: 5 ,
            backgroundColor: Colors.black ,
            textColor: Colors.white ,
            fontSize: 16.0
        );
        Navigator.of(context).pushAndRemoveUntil(MaterialPageRoute(
            builder: (BuildContext context) => MainScreen()), (
            Route<dynamic> route) => false);
      });
    }
    else{
      Fluttertoast.showToast(msg: "error");
    }
  }
}
