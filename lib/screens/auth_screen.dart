import 'dart:ui';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:urben/components/main_screen.dart';
import 'package:urben/screens/login_screen.dart';
import 'package:urben/screens/signup_screen.dart';

class AuthScreen extends StatefulWidget {
  @override
  _AuthScreenState createState() => _AuthScreenState();
}

class _AuthScreenState extends State<AuthScreen> {

  SharedPreferences sharedPreferences;

  @override
  void initState(){
    super.initState();
    checkLoginStatus();
  }

  checkLoginStatus() async {
    sharedPreferences = await SharedPreferences.getInstance();
    if(sharedPreferences.getString("token")!=null){
      Navigator.of(context).pushAndRemoveUntil(MaterialPageRoute(
          builder: (BuildContext context) => MainScreen()), (
          Route<dynamic> route) => false);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xff242633),
      body: Padding(
        padding: EdgeInsets.only(left: 30, right: 30, bottom: 60),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Spacer(flex: 1,),
            Text(
              'Urben',
                style: TextStyle(
                    fontSize: 60,
                    color: Colors.red.shade700,
                    letterSpacing: 4,
                    fontWeight: FontWeight.w300),
            ),
            Text(
              'LIVE ADS',
              style: TextStyle(
                  fontSize: 30,
                  letterSpacing: 2,
                  fontWeight: FontWeight.w400,
                  color: Colors.white),
            ),
            SizedBox(height: 80,),
            SizedBox(
              width: double.infinity,
              child: RaisedButton(
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(5)),
                padding: const EdgeInsets.all(20),
                color: Colors.white,
                child: Text(
                  "LOGIN",
                  style: TextStyle(
                    color: Colors.grey.shade800,
                    fontWeight: FontWeight.w400,
                    fontSize: 18,
                  ),
                ),
                onPressed: () {
                  Navigator.of(context).push(MaterialPageRoute(builder: (context) => LoginScreen()));
                },
              ),
            ),
            SizedBox(height: 20),
            SizedBox(
              width: double.infinity,
              child: RaisedButton(
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(5.0),
                  side: BorderSide(color: Colors.lightBlueAccent,width: 2.5,style: BorderStyle.solid)
                ),
                padding: const EdgeInsets.all(20),
                color: Color(0xff242633),
                child: Text(
                  "SIGN UP",
                  style: TextStyle(
                      color: Colors.lightBlueAccent,
                      fontSize: 18,
                    fontWeight: FontWeight.w400,
                  ),
                ),
                onPressed: () {
                  Navigator.of(context).push(MaterialPageRoute(builder: (context) => SignUpScreen()));
                },
              ),
            ),
          ],
        ),
      ),
    );
  }
}
