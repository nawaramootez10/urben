import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class AnalyticsScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            Row(
              children: <Widget>[
                Padding(
                  padding: EdgeInsets.all(10),
                  child: Container(
                    height: 70,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.all(Radius.circular(15)),
                      gradient: LinearGradient(
                        begin: Alignment(1.0, -1.0),
                        end: Alignment(-1.0, 1.0),
                        colors: [
                          const Color(0xffefbad3),
                          const Color(0xffa254f2)
                        ],
                        stops: [0.0, 1.0],
                      ),
                    ),
                    child: FlatButton(
                      onPressed: null,
                        child: Column(
                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            children: [Text('Total sales'), Text('2500.00')])),
                  ),
                ),
                Padding(
                  padding: EdgeInsets.all(10),
                  child: Container(
                    height: 70,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.all(Radius.circular(15)),
                      gradient: LinearGradient(
                        begin: Alignment(1.0, -1.0),
                        end: Alignment(-1.0, 1.0),
                        colors: [
                          const Color(0xfffec180),
                          const Color(0xffff8993)
                        ],
                        stops: [0.0, 1.0],
                      ),
                    ),
                    child: FlatButton(
                      onPressed: null,
                        child: Column(
                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            children: [Text('Leads'), Text('3840')])),
                  ),
                ),
                Padding(
                  padding: EdgeInsets.all(10),
                  child: Container(
                    height: 70,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.all(Radius.circular(15)),
                      gradient: LinearGradient(
                        begin: Alignment(1.0, -1.0),
                        end: Alignment(-1.0, 1.0),
                        colors: [
                          const Color(0xffd0ffae),
                          const Color(0xff34ebe9)
                        ],
                        stops: [0.0, 1.0],
                      ),
                    ),
                    child: FlatButton(
                      onPressed: null,
                        child: Column(
                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            children: [Text('Conversion'), Text('70%')])),
                  ),
                )
              ],
            ),
            Padding(
              padding: EdgeInsets.all(12),
              child: Container(
                child: RichText(
                    text: TextSpan(children: [
                      TextSpan(
                        text: '£4800.00',
                        style: TextStyle(
                          color: Color(0xfffec180),
                          fontSize: 40,
                          fontFamily: 'Bebas',
                          fontWeight: FontWeight.normal,
                        ),
                      ),
                    ])),
              ),
            ),
            Padding(
              padding: EdgeInsets.all(0),
              child: Container(
                child: RichText(
                    text: TextSpan(children: [
                      TextSpan(
                        text: 'Total balance'.toUpperCase(),
                        style: TextStyle(
                          color: Colors.grey,
                          fontSize: 20,
                          fontFamily: 'Bebas',
                          fontWeight: FontWeight.normal,
                        ),
                      ),
                    ])),
              ),
            ),
            Padding(
              padding: EdgeInsets.only(right: 120, top: 30),
              child: Container(
                child: RichText(
                    text: TextSpan(children: [
                      TextSpan(
                        text: 'Last Records Overview',
                        style: TextStyle(
                          color: Colors.grey,
                          fontSize: 20,
                          fontFamily: 'Bebas',
                          fontWeight: FontWeight.normal,
                        ),
                      ),
                    ])),
              ),
            ),
            SizedBox(height: 25,),
            ListTile(
                leading: Padding(
                  padding: EdgeInsets.all(0),
                  child: Container(
                    height: 90,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.all(Radius.circular(15)),
                      gradient: LinearGradient(
                        begin: Alignment(1.0, -1.0),
                        end: Alignment(-1.0, 1.0),
                        colors: [
                          const Color(0xfffec180),
                          const Color(0xffff8993)
                        ],
                        stops: [0.0, 1.0],
                      ),
                    ),
                    child: FlatButton(
                      onPressed: null,
                        child: Icon(FontAwesomeIcons.shoppingCart)),
                  ),
                ),
                title: Column(children: [Text('Sales',style: TextStyle(fontSize: 20),), Text("Marketplases",style: TextStyle(fontSize: 15,color: Colors.grey))]),
                trailing: Padding(
                    padding: EdgeInsets.all(12),
                    child: Column(children: [Text('Today',style: TextStyle(color: Colors.grey)), Text("£2500.000",style: TextStyle(color: Color(0xfffec180),),)])
                )
            ),
            SizedBox(height: 25,),
            ListTile(
                leading: Padding(
                  padding: EdgeInsets.all(0),
                  child: Container(
                    height: 70,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.all(Radius.circular(15)),
                      gradient: LinearGradient(
                        begin: Alignment(1.0, -1.0),
                        end: Alignment(-1.0, 1.0),
                        colors: [
                          const Color(0xffefbad3),
                          const Color(0xffa254f2)
                        ],
                        stops: [0.0, 1.0],
                      ),
                    ),
                    child: FlatButton(
                      onPressed: null,
                        child: Icon(FontAwesomeIcons.tshirt)),
                  ),
                ),
                title: Column(children: [Text('Products',style: TextStyle(fontSize: 20),), Text("On wharehouse",style: TextStyle(fontSize: 15,color: Colors.grey))]),
                trailing: Padding(
                    padding: EdgeInsets.all(12),
                    child: Column(children: [Text('03/04/2018',style: TextStyle(color: Colors.grey)), Text("345",style: TextStyle(color: Color(0xffefbad3),),)])
                )
            ),
            SizedBox(height: 25,),
            ListTile(
                leading: Padding(
                  padding: EdgeInsets.all(0),
                  child: Container(
                    height: 70,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.all(Radius.circular(15)),
                      gradient: LinearGradient(
                        begin: Alignment(1.0, -1.0),
                        end: Alignment(-1.0, 1.0),
                        colors: [
                          const Color(0xffd0ffae),
                          const Color(0xff34ebe9)
                        ],
                        stops: [0.0, 1.0],
                      ),
                    ),
                    child: FlatButton(
                      onPressed: null,
                        child: Icon(FontAwesomeIcons.home)),
                  ),
                ),
                title: Column(children: [Text('Page Views',style: TextStyle(fontSize: 20),), Text("On app",style: TextStyle(fontSize: 15,color: Colors.grey))]),
                trailing: Padding(
                    padding: EdgeInsets.all(12),
                    child: Column(children: [Text('01/03/2018',style: TextStyle(color: Colors.grey)), Text("432910",style: TextStyle(color:  Color(0xffd0ffae),))])
                )
            ),
          ],
        ),
      ),
    );
  }
}
