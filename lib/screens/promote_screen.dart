import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:urben/models/product.dart';
import 'package:http/http.dart' as http;

class PromoteScreen extends StatefulWidget {
  @override
  _PromoteScreenState createState() => _PromoteScreenState();
}

class _PromoteScreenState extends State<PromoteScreen> {
  var authToken, productName, productDescription, productCategory, productTags, productImage, productInitialPrice, productOnSalePrice, productVisibility, status;

  List unfilterData;
  Future<List<Product>> _getProducts() async {
    SharedPreferences sharedPreferences = await SharedPreferences.getInstance();
    authToken = sharedPreferences.getString("token");
    print(authToken);
    var data = await http.get(
        "http://10.0.2.2:5000/api/v1/test",
        headers: {'Authorization': 'Bearer '+authToken});
    var jsonData = json.decode(data.body);
    this.unfilterData = jsonData;
    print(jsonData);
    List<Product> products = [];
    for (var u in jsonData) {
      Product product = Product(u["productName"], u["productDescription"], u["productCategory"], u["productTags"], u["productImage"],
          u["productInitialPrice"], u["productOnSalePrice"], u["productVisibility"], u["status"] );
      if(product.status==true){
        products.add(product);
      }
    }
    print(products.length);
    return products;
  }

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: false,
        title: Text(
          " Promoted Products ",
          style: TextStyle(color: Colors.black, fontWeight: FontWeight.bold),
        ),
        backgroundColor: Colors.white,
        elevation: 2,
      ),
      body: new Container(
        margin: EdgeInsets.all(10.0),
        child: FutureBuilder(
          future: _getProducts(),
          builder: (BuildContext context, AsyncSnapshot snapshot) {
            if (snapshot.data == null) {
              return new Container(
                  margin: const EdgeInsets.only(top: 100.0),
                  child: new Center(
                      child: new Column(
                        children: <Widget>[
                          new CircularProgressIndicator(strokeWidth: 4.0),
                          new Container(
                            padding: const EdgeInsets.all(8.0),
                            child: new Text(
                              'Chargement',
                              style: new TextStyle(
                                  color: Colors.blueGrey.shade300,
                                  fontSize: 16.0),
                            ),
                          )
                        ],
                      )
                  )
              );
            } else {
              if (snapshot.data.length == 0) {
                return Container(
                  margin: const EdgeInsets.all(50.0),
                   child: Text("Aucun produit en promotion", style: TextStyle(color: Colors.red, fontWeight: FontWeight.w500, fontSize: 22,),),
                );
              } else {
                return Scrollbar(
                  child: new Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: GridView.builder(
                      shrinkWrap: true,
                      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                          crossAxisCount: 1),
                      itemBuilder: (BuildContext context, int index) {
                        return Padding(
                            padding: EdgeInsets.all(5.0),
                            child: GestureDetector(
                              child: Container(
                                  decoration: BoxDecoration(
                                      color: Colors.white,
                                      shape: BoxShape.rectangle,
                                      borderRadius: BorderRadius.circular(20.0),
                                      boxShadow: [
                                        BoxShadow(
                                            color: Colors.black12,
                                            blurRadius: 8.0)
                                      ]),
                                  child: Column(
                                    crossAxisAlignment: CrossAxisAlignment
                                        .start,
                                    children: <Widget>[
                                      Container(
                                        height: 200.0,
                                        padding: EdgeInsets.all(10.0),
                                        child: Row(
                                          crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                          children: <Widget>[
                                            Expanded(
                                              child: Container(
                                                child: new ClipRRect(
                                                  borderRadius:
                                                  new BorderRadius
                                                      .circular(15.0),
                                                  child: Image.network(
                                                    // snapshot
                                                    //     .data[index].productImage,
                                                    "https://images.unsplash.com/photo-1512106374988-c95f566d39ef?ixlib=rb-1.2.1&auto=format&fit=crop&w=2000&q=80",
                                                    fit: BoxFit.cover,
                                                    height: 200.0,
                                                    width: 500.0,
                                                  ),
                                                ),
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                      Padding(
                                        padding: EdgeInsets.only(
                                          left: 15.0,
                                          right: 15.0,),
                                        child: Column(
                                          crossAxisAlignment: CrossAxisAlignment
                                              .start,
                                          children: <Widget>[
                                            Text(
                                              snapshot.data[index].productName,
                                              style: TextStyle(
                                                  fontWeight: FontWeight.w600,
                                                  fontSize: 20.0,
                                                  color: Colors.teal),
                                            ),
                                            SizedBox(height: 10,),
                                            Text(
                                              "Category:  " +
                                                  snapshot.data[index]
                                                      .productCategory,
                                              style: TextStyle(
                                                  fontWeight: FontWeight.w600,
                                                  fontSize: 18.0,
                                                  color: Colors.grey),
                                            ),
                                            SizedBox(height: 10,),
                                            Text(
                                              "Discount:  10%",
                                              style: TextStyle(
                                                  fontWeight: FontWeight.w600,
                                                  fontSize: 18.0,
                                                  color: Colors.grey),
                                            ),
                                          ],
                                        ),
                                      ),
                                    ],
                                  )),
                            ));
                      },
                      itemCount: snapshot.data.length,
                    ),
                  ),
                );
              }
            }
          },
        ),
      ),
    );
  }
}

