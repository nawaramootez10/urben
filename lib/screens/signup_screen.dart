import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:urben/screens/login_screen.dart';
import 'package:http/http.dart' as http;


class SignUpScreen extends StatefulWidget {
  @override
  _SignUpScreenState createState() => _SignUpScreenState();
}

class _SignUpScreenState extends State<SignUpScreen> {

  TextEditingController managerNameController = new TextEditingController();
  TextEditingController workspaceNameController = new TextEditingController();
  TextEditingController categoryController = new TextEditingController();
  TextEditingController descriptionController = new TextEditingController();
  TextEditingController emailController = new TextEditingController();
  TextEditingController passwordController = new TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Container(
          padding: EdgeInsets.all(20.0),
          color: Color(0xff242633),
          child: ListView(
            children: <Widget>[
              Column(
                children: <Widget>[
                  SizedBox(height: 0,),
                  Text(
                    'Urben',
                    style: TextStyle(
                      fontSize: 60,
                      fontWeight: FontWeight.w300,
                      color: Colors.red),
                  ),
                  SizedBox(height: 15,),
                  Text(
                      'Sign Up',
                      style: TextStyle(
                          fontSize: 35,
                          fontWeight: FontWeight.w400,
                          color: Colors.white),
                  ),
                  SizedBox(height: 15,),
                  ListTile(
                      title: TextField(
                        controller: managerNameController,
                        cursorColor: Colors.white,
                        style: TextStyle(color: Colors.white),
                        decoration: InputDecoration(
                            hintText: "Manager Name",
                            hintStyle: TextStyle(color: Colors.white38),
                            border: InputBorder.none,
                            icon: Icon(Icons.person, color: Colors.white,)
                        ),
                      )
                  ),
                  Divider(color: Colors.white,),
                  ListTile(
                      title: TextField(
                        controller: workspaceNameController,
                        cursorColor: Colors.white,
                        style: TextStyle(color: Colors.white),
                        decoration: InputDecoration(
                            hintText: "Workspace Name",
                            hintStyle: TextStyle(color: Colors.white38),
                            border: InputBorder.none,
                            icon: Icon(Icons.local_convenience_store, color: Colors.white,)
                        ),
                      )
                  ),
                  Divider(color: Colors.white,),
                  ListTile(
                      title: TextField(
                        controller: categoryController,
                        cursorColor: Colors.white,
                        style: TextStyle(color: Colors.white),
                        decoration: InputDecoration(
                            hintText: "Category",
                            hintStyle: TextStyle(color: Colors.white38),
                            border: InputBorder.none,
                            icon: Icon(Icons.category, color: Colors.white,)
                        ),
                      )
                  ),
                  Divider(color: Colors.white,),
                  ListTile(
                      title: TextField(
                        controller: descriptionController,
                        cursorColor: Colors.white,
                        style: TextStyle(color: Colors.white),
                        decoration: InputDecoration(
                            hintText: "Description",
                            hintStyle: TextStyle(color: Colors.white38),
                            border: InputBorder.none,
                            icon: Icon(Icons.description, color: Colors.white,)
                        ),
                      )
                  ),
                  Divider(color: Colors.white,),
                  ListTile(
                      title: TextField(
                        controller: emailController,
                        cursorColor: Colors.white,
                        style: TextStyle(color: Colors.white),
                        decoration: InputDecoration(
                            hintText: "Email Address",
                            hintStyle: TextStyle(color: Colors.white38),
                            border: InputBorder.none,
                            icon: Icon(Icons.email, color: Colors.white,)
                        ),
                      )
                  ),
                  Divider(color: Colors.white,),
                  ListTile(
                      title: TextField(
                        controller: passwordController,
                        cursorColor: Colors.white,
                        obscureText: true,
                        style: TextStyle(color: Colors.white),
                        decoration: InputDecoration(
                            hintText: "Password",
                            hintStyle: TextStyle(color: Colors.white38),
                            border: InputBorder.none,
                            icon: Icon(Icons.lock_outline, color: Colors.white,)
                        ),
                      )
                  ),
                  Divider(color: Colors.white,),
                  ListTile(
                      title: TextField(
                        cursorColor: Colors.white,
                        obscureText: true,
                        style: TextStyle(color: Colors.white),
                        decoration: InputDecoration(
                            hintText: "Confirm Password:",
                            hintStyle: TextStyle(color: Colors.white38),
                            border: InputBorder.none,
                            icon: Icon(Icons.lock_outline, color: Colors.white,)
                        ),
                      )
                  ),
                  SizedBox(height: 30,),
                  SizedBox(
                    width: double.infinity,
                    child: RaisedButton(
                        padding: const EdgeInsets.all(16.0),
                        onPressed: (){
                          signUp(managerNameController.text,workspaceNameController.text,categoryController.text,descriptionController.text,emailController.text, passwordController.text);
                        },
                        color: Colors.white,
                        child: Text('CREATE ACCOUNT', style: TextStyle(color: Colors.black, fontSize: 16.0),),
                    ),
                  ),
                  SizedBox(height: 5,),
                  SizedBox(
                    width: double.infinity,
                    child: RaisedButton(
                      onPressed: (){Navigator.of(context).push(MaterialPageRoute(builder: (context) => LoginScreen()));},
                      color: Color(0xff242633),
                      child: Text('have an account? Sign In', style: TextStyle(color: Colors.white, fontSize: 16),),
                    ),
                  ),
                ],
              ),
            ],
          ),
        )
    );
  }
  signUp(String managerName, String workspaceName, String category, String description, String email,String password) async {
    Map data = {
      'managerName': managerName,
      'workspaceName': workspaceName,
      'category': category,
      'description': description,
      'email': email,
      'password': password
    };
    var jsonData;
    var response = await http.post(
        "http://10.0.2.2:5000/api/v1/register", body: data);
    if (response.statusCode == 200) {
      jsonData = json.decode(response.body);
      setState(() {
        Navigator.of(context).pushAndRemoveUntil(MaterialPageRoute(
            builder: (BuildContext context) => LoginScreen()), (
            Route<dynamic> route) => false);
      });
    }
    else{
      Fluttertoast.showToast(msg: "error");
    }
  }
}
