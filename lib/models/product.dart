class Product {
  String productName;
  String productDescription;
  String productCategory;
  String productTags;
  String productImage;
  num productInitialPrice;
  num productOnSalePrice;
  String productVisibility;
  bool status;

  Product(this.productName,this.productDescription, this.productCategory, this.productTags, this.productImage,
      this.productInitialPrice, this.productOnSalePrice, this.productVisibility, this.status);
  Product.fromJson(Map json)
      : productName = json['productName'],
        productDescription = json['productDescription'],
        productCategory = json['productCategory'],
        productTags = json['productTags'],
        productImage = json['productImage'],
        productInitialPrice = json['productInitialPrice'],
        productOnSalePrice = json['productOnSalePrice'],
        productVisibility = json['productVisibility'],
        status = json['status'];

  Map toJson() {
    return {
      'productName': productName,
      'productDescription': productDescription,
      'productCategory': productCategory,
      'productTags': productTags,
      'productImage': productImage,
      'productInitialPrice': productInitialPrice,
      'productOnSalePrice': productOnSalePrice,
      'productVisibility': productVisibility,
      'status': status,
    };
  }


}